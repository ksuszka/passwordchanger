Introduction
------------

*PasswordChanger* is a simple tool which can be used to change windows domain password for given user.

It has two major features:

- Credentials can be cached in encrypted local store.
- Different password can be set given number of times before final password is set.

*PasswordChanger* can be used to overcome problem of too restricted domain policy which forces frequent password changes.

Requirements and restrictions
------------

- .Net Framework 3.5 is required to run *PasswordChanger*

References
----------

*PasswordChanger* uses:

- [MSBuildVersioningTask] (to maintain source code version numbers)

Usage
-----

    Usage:
      PasswordChanger [options]
	  
    Where options are:
      -d, --domain         Active directory domain.
      -u, --username       Account name.
      -p, --password       Current user password.
      -n, --newpassword    Password to be set.
      -r, --repeat         How many times the password set operation should be repeated.
      -x, --nocache        Do not use credentials cache.


[MSBuildVersioningTask]: http://versioning.codeplex.com/
