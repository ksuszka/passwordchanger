@echo off
set PCPATH=%~dps0\PasswordChanger.exe
set LOG=%~dps0\PasswordChanger.log
echo Running PasswordChanger for the first time...
%PCPATH%
echo Scheduling PasswordChanger task...
set /p taskuser=User for running scheduled task: 
schtasks /create /SC WEEKLY /D MON /ST 03:33 /F /TN PasswordChanger /TR "cmd /c ^>^> %LOG% %PCPATH% -r 1" /RU %taskuser% /RP
echo Running PasswordChanger task...
schtasks /run /TN PasswordChanger
echo IMPROTANT: Please check PasswordChanger.log file to see if PasswordChecker task was executed successfully.
echo Finished.
