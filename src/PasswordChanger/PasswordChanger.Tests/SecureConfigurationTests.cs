﻿namespace PasswordChanger.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PasswordChanger.Utilities;

    /// <summary>
    /// This is a test class for SecureConfigurationTest and is intended
    /// to contain all SecureConfigurationTest Unit Tests
    /// </summary>
    [TestClass]
    public class SecureConfigurationTests
    {
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// 
        ////You can use the following additional attributes as you write your tests:
        ////
        ////Use ClassInitialize to run code before running the first test in the class
        ////[ClassInitialize()]
        ////public static void MyClassInitialize(TestContext testContext)
        ////{
        ////}
        ////
        ////Use ClassCleanup to run code after all tests in a class have run
        ////[ClassCleanup()]
        ////public static void MyClassCleanup()
        ////{
        ////}
        ////
        ////Use TestInitialize to run code before running each test
        ////[TestInitialize()]
        ////public void MyTestInitialize()
        ////{
        ////}
        ////
        ////Use TestCleanup to run code after each test has run
        ////[TestCleanup()]
        ////public void MyTestCleanup()
        ////{
        ////}
        ////
        #endregion

        [TestMethod]
        public void Load_ShouldLoadProperData_WhenDataWereSaved()
        {
            // Given
            string localPath = Guid.NewGuid().ToString();
            string key1 = "Key1";
            string value1 = "Value1";
            string key2 = "Key2";
            string value2 = "Value2";
            var gen = new SecureConfiguration(localPath);
            gen.Settings[key1] = value1;
            gen.Settings[key2] = value2;
            gen.Save();

            // When
            var sc = new SecureConfiguration(localPath);

            // Then
            Assert.AreEqual(value1, sc.Settings[key1]);
            Assert.AreEqual(value2, sc.Settings[key2]);
        }
    }
}
