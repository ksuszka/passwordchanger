﻿namespace PasswordChanger.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PasswordChanger.Utilities;

    /// <summary>
    /// This is a test class for CommandLineParserTest and is intended
    /// to contain all CommandLineParserTest Unit Tests.
    /// </summary>
    [TestClass]
    public class CommandLineParserTests
    {
        private TestContext testContextInstance;

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext
        {
            get
            {
                return this.testContextInstance;
            }

            set
            {
                this.testContextInstance = value;
            }
        }

        #region Additional test attributes
        //// 
        ////You can use the following additional attributes as you write your tests:
        ////
        ////Use ClassInitialize to run code before running the first test in the class
        ////[ClassInitialize()]
        ////public static void MyClassInitialize(TestContext testContext)
        ////{
        ////}
        ////
        ////Use ClassCleanup to run code after all tests in a class have run
        ////[ClassCleanup()]
        ////public static void MyClassCleanup()
        ////{
        ////}
        ////
        ////Use TestInitialize to run code before running each test
        ////[TestInitialize()]
        ////public void MyTestInitialize()
        ////{
        ////}
        ////
        ////Use TestCleanup to run code after each test has run
        ////[TestCleanup()]
        ////public void MyTestCleanup()
        ////{
        ////}
        ////
        #endregion

        [TestMethod]
        [ExpectedException(typeof(CommandLineParserException), "Unknown argument x")]
        public void Parse_ShouldThrowException_WhenUnknownArgumentIsGiven()
        {
            // Given
            var parser = new CommandLineParser();

            // When
            parser.Parse(new[] { "a", "b" });

            // Then
            // Exception thrown
        }

        [TestMethod]
        public void Parse_ShouldCallProperCallback_WhenArgumentWithNoParameteresIsGiven()
        {
            // Given
            var parser = new CommandLineParser();
            bool called = false;
            parser.Add("-x", () => called = true);
            parser.Add("-y", () => { });

            // When
            parser.Parse(new[] { "-x" });

            // Then
            Assert.IsTrue(called, "Callback was called.");
        }

        [TestMethod]
        public void Parse_ShouldCallCallbackWithProperParameters_WhenArgumentWithMultipleParameteresIsGiven()
        {
            // Given
            var parser = new CommandLineParser();
            string p1 = null, p2 = null;
            parser.Add("-x", (a, b) => { p1 = a; p2 = b; });

            // When
            parser.Parse(new[] { "-x", "test1", "test2" });

            // Then
            Assert.AreEqual("test1", p1);
            Assert.AreEqual("test2", p2);
        }

        [TestMethod]
        [ExpectedException(typeof(CommandLineParserException), "Not enough arguments for parameter x")]
        public void Parse_ShouldThrowException_WhenNotEnoughParametersIsGiven()
        {
            // Given
            var parser = new CommandLineParser();
            parser.Add("-d", (x, y, z) => { });

            // When
            parser.Parse(new[] { "-d", "p1", "p2" });

            // Then
            // Exception thrown
        }
    }
}
