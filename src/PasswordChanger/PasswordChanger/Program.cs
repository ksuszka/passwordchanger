﻿namespace PasswordChanger
{
    using System;
    using System.DirectoryServices.AccountManagement;
    using System.Reflection;
    using PasswordChanger.Utilities;

    internal class Program
    {
        private static int Main(string[] args)
        {
            string domain = null;
            string username = null;
            string currentPassword = null;
            string newPassword = null;
            bool useCache = true;
            bool clearCache = false;
            int rollTimes = 1;

            var clp = new CommandLineParser();
            clp.Add("^(-d)|(--domain)$", x => domain = x).Description("Active directory domain.");
            clp.Add("^(-u)|(--username)$", x => username = x).Description("Account name.");
            clp.Add("^(-p)|(--password)$", x => currentPassword = x).Description("Current user password.");
            clp.Add("^(-n)|(--newpassword)$", x => newPassword = x).Description("Password to be set.");
            clp.Add("^(-r)|(--repeat)$", x => rollTimes = int.Parse(x)).Description(
                "How many times the password set operation should be repeated.");
            clp.Add("^(-x)|(--nocache)$", () => useCache = false).Description("Do not use credentials cache.");
            clp.Add("^(-c)|(--clearcache)$", () => clearCache = true).Description("Clear creadentials cache.");

            try
            {
                clp.Parse(args);

                if (clearCache)
                {
                    ClearCredentialsCache();
                }
                else
                {
                    ResetPassword(domain, username, currentPassword, newPassword, rollTimes, useCache);
                }

                return 0;
            }
            catch (CommandLineParserException clpe)
            {
                var oldColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Error: {0}", clpe.Message);
                Console.ForegroundColor = oldColor;

                Console.WriteLine();
                Console.Write(clp.GetUsageText(Assembly.GetExecutingAssembly()));
                return -1;
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }

                var oldColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine("Error: {0}", ex.Message);
                Console.ForegroundColor = oldColor;
                return -2;
            }
        }

        private static void ClearCredentialsCache()
        {
            SecureConfiguration.Default.Settings.Clear();
            SecureConfiguration.Default.Save();
            Console.WriteLine("Credentials cache was cleared.");
        }

        private static void ResetPassword(string domain, string username, string currentPassword, string newPassword, int rollTimes, bool useCache)
        {
            domain = GetParameter("Domain: ", domain, useCache);
            domain = string.IsNullOrEmpty(domain) ? null : domain;
            username = GetParameter("Username: ", username, useCache);
            username = string.IsNullOrEmpty(username) ? UserPrincipal.Current.SamAccountName : username;
            currentPassword = GetParameter("Current password: ", currentPassword, useCache, true);
            newPassword = GetParameter("New password: ", newPassword, useCache, true);

            var principalContext = new PrincipalContext(ContextType.Domain, domain);
            var userPrincipal = UserPrincipal.FindByIdentity(principalContext, IdentityType.UserPrincipalName, username)
                                ?? UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, username);
            if (userPrincipal == null)
            {
                throw new InvalidOperationException(string.Format("Cannot find user {0}.", username));
            }

            Console.WriteLine("Last password set date: {0}", userPrincipal.LastPasswordSet);
            for (int i = 1; i < rollTimes; i++)
            {
                string password = string.Format("{0}_{1}", newPassword, i);
                Console.WriteLine("Setting temporary password {0}...", i);
                userPrincipal.ChangePassword(currentPassword, password);
                currentPassword = password;
            }

            Console.WriteLine("Setting new password...");

            userPrincipal.ChangePassword(currentPassword, newPassword);
            userPrincipal = UserPrincipal.FindByIdentity(principalContext, IdentityType.Guid, userPrincipal.Guid.ToString());
            Console.WriteLine(
                "Last password set date: {0}", (userPrincipal != null) ? userPrincipal.LastPasswordSet.ToString() : "Unknown");

            if (useCache)
            {
                SecureConfiguration.Default.Save();
            }
        }

        private static string GetParameter(string name, string value, bool useCache, bool maskInput = false)
        {
            if (useCache && value == null)
            {
                value = SecureConfiguration.Default.Settings[name];
            }

            if (value == null)
            {
                value = maskInput ? ConsoleHelper.PromptUserForSecretText(name) : ConsoleHelper.PromptUser(name);
            }

            if (useCache && value != null)
            {
                SecureConfiguration.Default.Settings[name] = value;
            }

            return value;
        }
    }
}
