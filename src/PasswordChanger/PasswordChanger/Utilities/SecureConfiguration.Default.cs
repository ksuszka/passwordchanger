﻿namespace PasswordChanger.Utilities
{
    /// <summary>
    /// Provides method for loading and saving data in persistent encrypted store.
    /// </summary>
    public partial class SecureConfiguration
    {
        private static readonly SecureConfiguration DefaultConfiguration = new SecureConfiguration("DefaultConfiguration");

        /// <summary>
        /// Gets the singleton with default secure configuration instance.
        /// </summary>
        public static SecureConfiguration Default
        {
            get
            {
                return DefaultConfiguration;
            }
        }
    }
}
