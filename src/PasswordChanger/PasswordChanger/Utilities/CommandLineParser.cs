﻿namespace PasswordChanger.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Very simple parser for command line arguments.
    /// </summary>
    public class CommandLineParser
    {
        private readonly Dictionary<string, Option> arguments = new Dictionary<string, Option>();

        /// <summary>
        /// Public "fluent" interface for defining argument properties.
        /// </summary>
        public interface IOption
        {
            /// <summary>
            /// Set description shown when help message is generated.
            /// </summary>
            /// <param name="text">Description to be shown.</param>
            /// <returns>Same object (for fluent interface).</returns>
            IOption Description(string text);
        }

        public IOption Add(string pattern, Action action)
        {
            return this.AddDelegate(pattern, action);
        }

        public IOption Add(string pattern, Action<string> action)
        {
            return this.AddDelegate(pattern, action);
        }

        public IOption Add(string pattern, Action<string, string> action)
        {
            return this.AddDelegate(pattern, action);
        }

        public IOption Add(string pattern, Action<string, string, string> action)
        {
            return this.AddDelegate(pattern, action);
        }

        public IOption Add(string pattern, Action<string, string, string, string> action)
        {
            return this.AddDelegate(pattern, action);
        }

        public void Parse(string[] args)
        {
            int i = 0;
            while (i < args.Length)
            {
                string arg = args[i];
                bool found = false;
                foreach (var kv in this.arguments)
                {
                    if (Regex.IsMatch(arg, kv.Key))
                    {
                        found = true;
                        Delegate action = kv.Value.Action;
                        int parametersCount = action.Method.GetParameters().Length;
                        if (i + parametersCount >= args.Length)
                        {
                            throw new CommandLineParserException(string.Format("Not enough parameters for argument {0}.", arg));
                        }

                        action.DynamicInvoke(args.Skip(i + 1).Take(parametersCount).OfType<object>().ToArray());
                        i += parametersCount;
                    }
                }

                if (!found)
                {
                    throw new CommandLineParserException(string.Format("Unknown argument {0}.", arg));
                }

                ++i;
            }
        }

        public string GetUsageText(Assembly mainAssembly)
        {
            var sb = new StringBuilder();

            var assemblyName = mainAssembly.GetName();
            sb.AppendFormat("{0} version {1}\n\n", assemblyName.Name, assemblyName.Version);
            sb.AppendFormat("Usage:\n\n    {0} [options]\n\n", assemblyName.Name);

            if (this.arguments.Count > 0)
            {
                sb.AppendFormat("Where options are:\n\n");
                foreach (var option in this.arguments.Values)
                {
                    var displayOptions =
                        string.Join(", ", option.DisplayPattern.Split('|').Select(s => s.Trim("^()$ ".ToCharArray())).ToArray());
                    sb.AppendFormat("   {0,-24} {1}\n", displayOptions, option.Description);
                }

                sb.AppendFormat("\n");
            }
            
            return sb.ToString();
        }

        private IOption AddDelegate(string pattern, Delegate action)
        {
            var option = new Option(pattern, action);
            this.arguments.Add(pattern, option);
            return option;
        }

        private class Option : IOption
        {
            public Option(string pattern, Delegate action)
            {
                this.Pattern = pattern;
                this.Action = action;
            }

            public string Description { get; set; }

            public string Pattern { get; private set; }

            public Delegate Action { get; private set; }

            public string DisplayPattern
            {
                get
                {
                    return this.Pattern;
                }
            }

            IOption IOption.Description(string text)
            {
                this.Description = text;
                return this;
            }
        }
    }
}
