﻿namespace PasswordChanger.Utilities
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class CommandLineParserException : Exception
    {
        ////
        //// For guidelines regarding the creation of new exception types, see
        ////    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        //// and
        ////    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        ////

        public CommandLineParserException()
        {
        }

        public CommandLineParserException(string message)
            : base(message)
        {
        }

        public CommandLineParserException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected CommandLineParserException(
            SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
