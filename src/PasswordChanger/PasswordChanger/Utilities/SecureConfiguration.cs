﻿namespace PasswordChanger.Utilities
{
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;

    /// <summary>
    /// Provides method for loading and saving data in persistent encrypted store.
    /// </summary>
    public partial class SecureConfiguration
    {
        private readonly string localPath;

        public SecureConfiguration(string localPath)
        {
            this.localPath = localPath;
            this.Settings = new NameValueCollection();
            this.Load();
        }
        
        public NameValueCollection Settings { get; private set; }

        public void Load()
        {
            using (var storage = GetIsolatedUserStore())
            {
                try
                {
                    using (var stream = new IsolatedStorageFileStream(this.localPath, FileMode.Open))
                    {
                        using (var file = new BinaryReader(stream))
                        {
                            try
                            {
                                var encryptedData = file.ReadBytes((int)stream.Length);
                                var decryptedData = ProtectedData.Unprotect(encryptedData, null, DataProtectionScope.CurrentUser);
                                using (var ms = new MemoryStream(decryptedData))
                                {
                                    try
                                    {
                                        var serializer = new BinaryFormatter();
                                        this.Settings = (NameValueCollection)serializer.Deserialize(ms);
                                    }
                                    catch (SerializationException se)
                                    {
                                        Trace.TraceError("Invalid configuration file {0}: {1}", this.localPath, se);
                                    }
                                }
                            }
                            catch (CryptographicException ce)
                            {
                                Trace.TraceError("Invalid configuration file {0}: {1}", this.localPath, ce);
                            }
                        }
                    }
                }
                catch (FileNotFoundException fnfe)
                {
                    Trace.TraceWarning("Default file {0} was not found.", fnfe.FileName);
                }
            }
        }

        public void Save()
        {
            using (var storage = GetIsolatedUserStore())
            {
                using (var ms = new MemoryStream())
                {
                    var serializer = new BinaryFormatter();
                    serializer.Serialize(ms, this.Settings);
                    var encryptedData = ProtectedData.Protect(ms.ToArray(), null, DataProtectionScope.CurrentUser);
                    using (var stream = new IsolatedStorageFileStream(this.localPath, System.IO.FileMode.Create))
                    {
                        using (var file = new BinaryWriter(stream))
                        {
                            file.Write(encryptedData);
                        }
                    }
                }
            }
        }

        private static IsolatedStorageFile GetIsolatedUserStore()
        {
            return IsolatedStorageFile.GetUserStoreForAssembly();
        }
    }
}
