﻿namespace PasswordChanger.Utilities
{
    using System;
    using System.IO;
    using System.Text;

    public static class ConsoleHelper
    {
        public static bool IsConsoleAvailable
        {
            get
            {
                try
                {
                    bool dummy = Console.TreatControlCAsInput;
                }
                catch (IOException)
                {
                    return false;
                }

                return true;
            }
        }

        public static string ReadMaskedLine()
        {
            var text = new StringBuilder();
            while (true)
            {
                ConsoleKeyInfo info = Console.ReadKey(true);
                if (info.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }

                if (info.Key == ConsoleKey.Backspace)
                {
                    if (text.Length > 0)
                    {
                        text.Remove(text.Length - 1, 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    text.Append(info.KeyChar);
                    Console.Write("*");
                }
            }

            return text.ToString();
        }

        public static string PromptUser(string text)
        {
            Console.Write(text);
            if (!IsConsoleAvailable)
            {
                throw new InvalidOperationException("No interactive console is available!");
            }

            return Console.ReadLine();
        }

        public static string PromptUserForSecretText(string text)
        {
            Console.Write(text);
            if (!IsConsoleAvailable)
            {
                throw new InvalidOperationException("No interactive console is available!");
            }

            return ReadMaskedLine();
        }
    }
}
